#ifndef MODULE3_H
#define MODULE3_H

#include "common.h"

#ifdef TARGET_DLL
#define LINKTYPE __declspec(dllexport)
#else
#define LINKTYPE
#endif

LINKTYPE common_thing * module3(void);

#endif
